import sqlalchemy
from sqlalchemy import orm


from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import and_

Base = declarative_base()

def create_alchemy_engine():
    # TODO przerzucić to do jakiego pliku konfiguracyjnego
    settings = {'drivername': 'mysql', 'host': 'localhost', 'port': '3306', 'database': 'wordnet_work', 'username': 'root', 'password': 'root', 'query': {'charset': 'utf8mb4'}}
    url = sqlalchemy.engine.url.URL(**settings)
    engine = sqlalchemy.create_engine(url, echo=False)
    return engine

def create_alchemy_session(engine):
    session = sqlalchemy.orm.sessionmaker(bind=engine)
    return session()

def get_id(lemma, variant, session):
    if not variant:
        return None
    query = find_id(lemma, variant, session)
    for row in query.all():
        return row.id

def find_id(lemma, variant, session):
    return (session.query(LexicalUnit.id)
            .filter(and_(LexicalUnit.lemma == lemma, LexicalUnit.variant == variant)))

class LexicalUnit(Base):

    __tablename__ = 'lexicalunit'

    id = Column('id', Integer, primary_key=True)
    lemma = Column(String(255))
    variant = Column(Integer)