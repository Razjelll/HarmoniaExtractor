HARMONIA_FILENAME = "D:/Praca/sample.tsv"
OUTPUT_FILE = "D:/Praca/harmonia.txt"

POLISH_UNIT = 2
ENGLISH_UNIT = 3

import db_helper
def run():
    element_dict = read_harmonia_file(HARMONIA_FILENAME)
    write(element_dict, OUTPUT_FILE)
    pass


def read_harmonia_file(harmonia_filename):
    file = open(harmonia_filename, encoding='utf-8')
    content = [x.strip() for x in file.readlines()]
    head_line = True
    result_dict = []
    engine = db_helper.create_alchemy_engine()
    session = db_helper.create_alchemy_session(engine)
    for line in content:
        if line == '':
            continue
        if head_line:
            head_line = False
            continue
        line_parts = line.split('\t')
        if line_parts[POLISH_UNIT] =='' or line_parts[ENGLISH_UNIT] == '':
            continue
        pl_unit = line_parts[POLISH_UNIT]
        eng_unit = line_parts[ENGLISH_UNIT]

        polish_element = get_element(pl_unit, session)
        english_element = get_element(eng_unit, session)

        result_dict.append([polish_element, english_element])
    file.close()
    return result_dict

def get_element(text, session):
    split_text = text.split(' ')
    start_lemma = 0
    id = 0
    if text[0].isdigit():
        id = int(split_text[0])
        start_lemma = 1
    split_lenght = len(split_text)
    lemma = ''
    for lemma_part in split_text[start_lemma:split_lenght - 1]:
        if lemma != '':
            lemma += ' '
        lemma += lemma_part
    if  split_text[split_lenght - 1].isdigit():
        variant = int(split_text[split_lenght -1])
    else:
        variant = 0
    element = Unit(lemma, variant)
    if not id:
        id = db_helper.get_id(lemma, variant, session)
    if id:
        element.set_id(id)

    return element

def write(data, output_filename):
    file = open(output_filename, 'w')
    for list in data:
        pl_text = _get_text(list[0])
        eng_text = _get_text(list[1])
        if not pl_text or not eng_text:
            print()
        file.write(pl_text + " : " + eng_text + '\n')
    file.close()

def _get_text(unit):
    text = ''
    if unit.get_id() != 0:
        text = str(unit.get_id())
        text += ' '
    text += unit.get_lemma()
    text += ' '
    text += str(unit.get_variant())
    return text


class Unit:

    def __init__(self, lemma, variant):
        self._id = 0
        self._lemma = lemma
        self._variant = variant

    def get_id(self):
        return self._id

    def get_lemma(self):
        return self._lemma

    def get_variant(self):
        return self._variant

    def set_id(self, id):
        self._id = id

run()